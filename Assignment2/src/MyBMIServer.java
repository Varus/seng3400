/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * This is a web service that provides unauthenticated functions for a client. The client has the ability to calculate
 * their BMI, request the BMI ranges known to the calculator and the range of ideal weights for a given height.
 */

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class MyBMIServer {

    /**
     * Member variables.
     */
    private MyBMIInformation BMIInformation = MyBMIInformation.getInstance();

    /**
     * Sends a response back to the client.
     *
     * @param response The response to send to the client.
     * @return The response.
     */
	private String sendResponse(String response) {
		// increments the call count on the BMI server
        // returns the response to the client
        BMIInformation.incCallCount();
		return response;
	}

	/**
	 * Calculates the BMI of a person given their weight and height.
	 *
	 * @param weight The weight of the person in kilograms.
	 * @param height The height of the person in centimetres.
	 * @return The BMI of the person or UNDEFINED if no range has been specified for the given weight
	 * and height.
	 */
	@WebMethod
	public String calcBMI(String weight, String height) {
		try {
            // convert the weight parameter to a double
            // convert the height parameter to a double in metres
            double w = Double.parseDouble(weight);
            double h = convertHeight(Double.parseDouble(height));
            // find the BMI range with the specified width and height
            // send the BMI range response to the client
            MyBMIServerRange BMIRange = BMIInformation.getBMIRanges().findBMIRange(w, h);
            return this.sendResponse(BMIRange == null ? MyBMIInformation.UNDEFINED : BMIRange.toString());
        } catch (NumberFormatException e) {
            return MyBMIInformation.UNDEFINED; // the BMI could not be calculated.
        }
	}

	/**
	 * Provides the BMI ranges known to the calculator.
	 *
	 * @return The BMI ranges.
	 */
	@WebMethod
	public String listRanges() {
		return this.sendResponse(BMIInformation.getBMIRanges().toString());
	}

	/**
	 * Provides the ideal range of weights for a given height.
	 *
	 * @param height The height of the person in metres.
	 * @return The ideal range of weights for the given height.
	 */
	@WebMethod
	public String listWeights(String height) {
        // convert the height parameter to a double in metres
        // return the list of weights
		double h = convertHeight(Double.parseDouble(height));
		return this.sendResponse(BMIInformation.getBMIRanges().getWeights(h));
	}

    /**
     * Converts a height from centimetres to metres.
     *
     * @param height The height in centimetres.
     * @return The height in metres.
     */
    private double convertHeight(double height) {
        return height / 100;
    }

}
