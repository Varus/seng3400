/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * This is a class that provides functionality for storing a list of BMI ranges.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyBMIServerRanges {

    private ArrayList<MyBMIServerRange> BMIRanges;

    /**
     * The default constructor for the BMI ranges.
     */
    public MyBMIServerRanges() {
        BMIRanges = new ArrayList<>();
    }

    /**
     * Retrieves the BMI range of a person given their weight and height.
     *
     * @param weight The weight of the person in kilograms.
     * @param height The height of the person in metres.
     * @param BMI The BMI of the person.
     * @return The name of the classification.
     */
    public String findClassification(double weight, double height, double BMI) {
        for (MyBMIServerRange BMIRange : BMIRanges) {
            // checks if the BMI falls within the current range and returns it
            if (BMIRange.insideBMIRange(BMI)) {
                return BMIRange.getName().toUpperCase();
            }
        }
        return null; // no range could be found
    }

    /**
     * Retrieves the BMI range of a specified name.
     *
     * @param name The name of the BMI range to retrieve.
     * @return The BMI range of a specified name.
     */
    public MyBMIServerRange getBMIRange(String name) {
        // loop through each BMI range
        for (MyBMIServerRange BMIRange : BMIRanges) {
            // checks if the BMI matches the name and returns it if located
            if (BMIRange.getName().equals(name)) {
                return BMIRange;
            }
        }
        return null; // no range could be found
    }

    /**
     * Provides the ideal range of weights for a given height.
     *
     * @param height The height of the person in metres.
     * @return The ideal range of weights for the given height.
     */
    public String getWeights(double height) {
        for (MyBMIServerRange BMIRange : BMIRanges) {
            // check if the range is ideal
            if (BMIRange.isDesirable()) {
                // calculate the normal weight ranges by providing the height
                return BMIRange.getWeightRange(MyBMIInformation.UNLIMITED, height);
            }
        }
        return String.format("%s\n", MyBMIInformation.UNDEFINED);
    }

    /**
     * Adds a given BMI range to the list.
     *
     * @param BMIRange the BMI range to add to the list.
     */
    public void add(MyBMIServerRange BMIRange) {
        BMIRanges.add(BMIRange);
        Collections.sort(BMIRanges, new Comparator<MyBMIServerRange>() {
            @Override
            public int compare(MyBMIServerRange r1, MyBMIServerRange r2) {
                return Double.compare(r1.getLower(), r2.getLower());
            }
        });
    }

    /**
     * Removes a BMI range from the list.
     *
     * @param BMIRange the BMI range to remove from the list.
     */
    public void remove(MyBMIServerRange BMIRange) {
        BMIRanges.remove(BMIRange);
    }

    /**
     * Checks if the lower and upper bound overlaps with an already defined BMI range.
     *
     * @param lower The lower bound to check against.
     * @param upper The upper bound to check against.
     * @return Whether there is an overlap with a defined BMI range.
     */
    public boolean hasOverlappingBMIRange(double lower, double upper) {
        // loops through each BMI range
        for (MyBMIServerRange BMIRange : BMIRanges) {
            // check if the range overlaps
            if (BMIRange.hasOverlap(lower, upper)) {
                return true;
            }
        }
        return false; // no overlap within any of the ranges
    }

    public boolean hasDesirableBMIRange() {
        // loops through each BMI range
        for (MyBMIServerRange BMIRange : BMIRanges) {
            // check if the range is ideal
            if (BMIRange.isDesirable()) {
                return true;
            }
        }
        return false; // no desirable BMI range was found
    }

    /**
     * An overridden method that returns a string representation of all BMI ranges.
     *
     * @return The string representation of the BMI ranges. If no ranges have been defined, an undefined
     * string is returned.
     */
    public String toString() {
        // check if any BMI ranges have been defined
        if (BMIRanges.size() == 0) {
            return MyBMIInformation.UNDEFINED;
        } else {
            // create a string buffer to store each range
            StringBuilder stringBuilder = new StringBuilder();
            // loop through each range and append it to the buffer
            for (MyBMIServerRange BMIRange : BMIRanges) {
                stringBuilder.append(BMIRange.toString());
            }
            return stringBuilder.toString();
        }
    }

}
