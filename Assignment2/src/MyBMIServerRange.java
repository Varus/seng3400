/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * This class stores the data that composes a BMI range and provides necessary functionality that is to be used in the
 * MyBMIServerRanges class.
 */

public class MyBMIServerRange {

    private String name;
    private double lower;
    private double upper;
    private boolean desirable;

    public static final double MAX_VALUE = Double.MAX_VALUE;
    public static final String FORMAT = "%.2f";

    /**
     * A constructor that defined a BMI range.
     *
     * @param name The name of the BMI range.
     * @param lower The lower bound of the BMI range.
     * @param upper The upper bound of the BMI range.
     * @param desirable Whether this BMI range is at a normal range.
     */
    public MyBMIServerRange(String name, double lower, double upper, boolean desirable) {
        this.name = name;
        this.lower = lower;
        this.upper = upper;
        this.desirable = desirable;
    }

    /**
     * Checks if the specified BMI falls within the BMI range.
     *
     * @param BMI The BMI to check against.
     */
    public boolean insideBMIRange(double BMI) {
        return BMI >= lower && BMI < upper;
    }

    /**
     * Checks if the lower and upper bound overlaps with the BMI range.
     *
     * @param lower The lower bound to check against.
     * @param upper The upper bound to check against.
     * @return Whether there is an overlap with the BMI range.
     */
    public boolean hasOverlap(double lower, double upper) {
        return lower >= this.lower && lower < this.upper || // lower bound is within the range
                upper > this.lower && upper < this.upper ||    	// upper bound is within the range
                lower <= this.lower && upper >= this.upper;     // encompasses entire range
    }

    /**
     * A mutator method that alters the name of the BMI range.
     *
     * @param name The new name of the BMI range.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * An accessor method that returns the name of the BMI range.
     *
     * @return The name of the BMI range.
     */
    public String getName() {
        return name;
    }

    /**
     * An accessor method that returns the lower bound of the BMI range.
     *
     * @return The lower bound of the BMI range.
     */
    public double getLower() {
        return lower;
    }

    /**
     * A method that determines whether the BMI range is desirable or not.
     */
    public boolean isDesirable() {
        return desirable;
    }

    /**
     * Returns a string representation of the weight range for the BMI range.
     *
     * @param unlimited The unlimited character denoted by an asterisk.
     */
    public String getWeightRange(String unlimited, double height) {
        String lower = this.lower == -MAX_VALUE ? unlimited : String.format(FORMAT, this.getWeight(this.lower, height));
        String upper = this.upper == MAX_VALUE ? unlimited : String.format(FORMAT, this.getWeight(this.upper, height));
        return String.format("%s - %s\n", lower, upper);
    }

    /**
     * Calculates and returns the weight given a particular BMI and height.
     *
     * @param BMI The BMI to check against.
     * @param height The height to check against.
     * @return The weight for a particular BMI and height.
     */
    private double getWeight(double BMI, double height) {
        return BMI * height * height;
    }

    /**
     * An overridden method that returns a string representation of a BMI range.
     *
     * @return The string representation of a BMI range.
     */
    @Override
    public String toString() {
        // create a new string buffer and convert the name to upper case
        // append the name to the string buffer
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s ", name.toUpperCase()));
        // determine which string to append to the buffer
        if (lower == -MAX_VALUE) {
            stringBuilder.append("<").append(String.format(FORMAT, upper));
        } else if (upper == MAX_VALUE) {
            stringBuilder.append(">").append(String.format(FORMAT, lower));
        } else {
            stringBuilder.append(String.format(FORMAT, lower));
            stringBuilder.append(" - ");
            stringBuilder.append(String.format(FORMAT, upper));
        }
        // return the string buffer with a new line escape character
        return stringBuilder.append("\n").toString();
    }

}
