/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * server.MyBMIInformation is a singleton class that is used for the purpose of sharing data between web services. It contains
 * the BMI ranges, call count and provides methods for accessing these and incrementing the call count.
 */

public class MyBMIInformation {

    /**
     * Member variables.
     */
    private static MyBMIInformation instance = null; // horrible singleton ahoy

    private MyBMIServerRanges BMIRanges;
    private int callCount;

    public static final String UNDEFINED = "UNDEFINED";
    public static final String UNLIMITED = "*";

    /**
     * A constructor to instantiate the singleton.
     */
    protected MyBMIInformation() {
        BMIRanges = new MyBMIServerRanges();
        callCount = 0;
    }

    /**
     * A method to return the singleton instance. It instantiates the singleton if it has not been declared.
     *
     * @return The singleton instance.
     */
    public static MyBMIInformation getInstance() {
        if (instance == null) {
            instance = new MyBMIInformation();
        }
        return instance;
    }

    /**
     * A method that calculates the BMU of a person given a weight and height.
     *
     * @param weight The weight of the person in kilograms.
     * @param height The height of the person in metres.
     * @return The BMI of the person.
     */
    public double calculateBMI(double weight, double height) {
        double BMI = weight / (height * height);
        return Double.parseDouble(String.format(MyBMIServerRange.FORMAT, BMI));
    }

    /**
     * An accessor method that returns the BMI ranges stored on the servers.
     *
     * @return The BMI ranges.
     */
    public MyBMIServerRanges getBMIRanges() {
        return BMIRanges;
    }

    /**
     * Increments the number of calls performed by the user.
     */
    public void incCallCount() {
        callCount++;
    }

    /**
     * An accessor method that returns the amount of calls performed by the user.
     *
     * @return The call count.
     */
    public int getCallCount() {
        return callCount;
    }

}
