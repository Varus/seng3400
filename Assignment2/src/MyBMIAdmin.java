/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * This is a web service that provides authenticated methods for an administrator. The administrator is able to add a
 * new BMI range, delete a BMI range, change the name of a BMI range or request the call count from all web services.
 */

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class MyBMIAdmin {

    /**
     * Member variables.
     */
    private MyBMIInformation BMIInformation = MyBMIInformation.getInstance();

    private final String USERNAME = "admin";    // horrible member variable 1
    private final String PASSWORD = "bodymass"; // horrible member variable 2

    /**
     * A method executed with each call.
     */
    private void performCall() {
        // increments the call count on the BMI server
        BMIInformation.incCallCount();
    }

    /**
     * Adds a new BMI range to the system.
     *
     * @param user The user to authenticate.
     * @param pwd The password given by the user.
     * @param lower The lower bound of the range.
     * @param upper The upper bound of the range.
     * @param name The name of the range.
     * @param normal Whether the range is a normal weight or not.
     * @return
     * 		False:
     *			- If a range with name already exists.
     *			- If the lower and upper bound overlap with a range that has already been defined.
     * 			- If normal is set and there is already another range defined as desirable.
     * 			- If the user credentials are invalid.
     * 		True:
     *			- If the new range has been added successfully.
     *		Notes:
     *			- The value of lower for a range can be the same as the value for upper for the adjoining range.
     *			- The value '*' denotes unlimited.
     *			- The desirable range is denoted as true for normal and false otherwise.
     */
    @WebMethod
    public boolean addRange(String user, String pwd, String lower, String upper, String name, boolean normal) {
        // performs the call and increments the call count
        this.performCall();
        // checks if the user could be authenticated
        if (this.authenticate(user, pwd)) {
            // retrieves the shared BMI ranges from the stored information
            MyBMIServerRanges BMIRanges = BMIInformation.getBMIRanges();
            // attempts to find the specified BMI range
            MyBMIServerRange BMIRange = BMIRanges.getBMIRange(name);
            // checks if the BMI range does not exist
            if (BMIRange == null) {
                // declare the unlimited string and the maximum range value
                String unlimited = MyBMIInformation.UNLIMITED; // *
                double maxValue = MyBMIServerRange.MAX_VALUE;
                // declare the lower and upper bounds of the BMI range
                double lowerBound;
                double upperBound;
                try {
                    // instantiate the lower and upper bound
                    lowerBound = lower.equals(unlimited) ? -maxValue : Double.parseDouble(lower);
                    upperBound = upper.equals(unlimited) ?  maxValue : Double.parseDouble(upper);
                } catch (NumberFormatException e) {
                    return false; // invalid data
                }
                // check if there is no overlap and no desirable range if normal is set
                if (!BMIRanges.hasOverlappingBMIRange(lowerBound, upperBound) &&
                        (!normal || !BMIRanges.hasDesirableBMIRange())) {
                    // add the new BMI range and return true
                    BMIRanges.add(new MyBMIServerRange(name, lowerBound, upperBound, normal));
                    return true;
                }
            }
        }
        return false; // the BMI range could not be added
    }

    /**
     * Attempts to delete a range in the system of a specified name.
     *
     * @param user The user to authenticate.
     * @param pwd The password given by the user.
     * @param name The name of the range to remove.
     * @return
     *		False: If the range with name does not exist or the user credentials are invalid.
     *		True: If the range has been removed successfully.
     */
    @WebMethod
    public boolean deleteRange(String user, String pwd, String name) {
        // performs the call and increments the call count
        this.performCall();
        // checks if the user could be authenticated
        if (this.authenticate(user, pwd)) {
            // retrieves the shared BMI ranges from the stored information
            MyBMIServerRanges BMIRanges = BMIInformation.getBMIRanges();
            // attempts to find the specified BMI range
            MyBMIServerRange BMIRange = BMIRanges.getBMIRange(name);
            // checks if the BMI range exists
            if (BMIRange != null) {
                // removes the BMI range and returns true
                BMIRanges.remove(BMIRange);
                return true;
            }
        }
        return false; // the BMI range was not deleted
    }

    /**
     * Updates the name of a specified range.
     *
     * @param user The user to authenticate.
     * @param pwd The password given by the user.
     * @param oldName The old name of the range.
     * @param newName The new name for the range.
     * @return
     *		False: If the range named oldName does not exist or the user credentials are invalid.
     *		True: If the range exists and its name has been updated.
     */
    @WebMethod
    public boolean setName(String user, String pwd, String oldName, String newName) {
        // performs the call and increments the call count
        this.performCall();
        // checks if the user could be authenticated
        if (this.authenticate(user, pwd)) {
            // attempts to find the specified BMI range
            MyBMIServerRange BMIRange = BMIInformation.getBMIRanges().getBMIRange(oldName);
            // checks if the BMI range exists
            if (BMIRange != null) {
                // alter the name of the BMI range to the new one and return true
                BMIRange.setName(newName);
                return true;
            }
        }
        return false; // the BMI range did not have its name changed
    }

    /**
     * Returns the total number of calls on either of the server interfaces, including the current call.
     *
     * @param user The user to authenticate.
     * @param pwd The password given by the user.
     * @return
     *		False: If the user credentials are invalid.
     * 		True: If the user credentials are valid.
     */
    @WebMethod
    public int callCount(String user, String pwd) {
        // performs the call and increments the call count
        // attempts to authenticate the user and returns either the call count or -1
        this.performCall();
        return this.authenticate(user, pwd) ? BMIInformation.getCallCount() : -1;
    }

    /**
     * Attempts to authenticate a user and the password that is provided by them.
     *
     * @param username The username of the user being authenticated.
     * @param password The password provided by the user.
     * @return Whether the user was authenticated or not.
     */
    private boolean authenticate(String username, String password) {
        return username.equals(USERNAME) && password.equals(PASSWORD);
    }

}
