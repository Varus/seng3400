/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * MyBMIClient is a command-line based client that enables the user to interact with the MyBMIServer web service.
 */

import localhost.axis.MyBMIServer_jws.*;
import java.rmi.RemoteException;

public class MyBMIClient {

    /**
     * The main method called upon execution.
     *
     * @param args Command line arguments passed in by the user.
     */
    public static void main(String[] args) {
        // instantiate the class and run the new main method
        MyBMIClient myBMIClient = new MyBMIClient();
        myBMIClient.runMyBMIClient(args);
    }

    /**
     * Runs the main program.
     *
     * @param args The command line arguments passed in from the main method.
     */
    public void runMyBMIClient(String[] args) {
        try {
            // instantiate the web service
            MyBMIServerServiceLocator locator = new MyBMIServerServiceLocator();
            MyBMIServer service = locator.getMyBMIServer();
            // get the method name from the arguments
            String methodName = args[0];
            // call the appropriate method
            switch (methodName) {
                case "calcBMI":
                    this.calcBMI(service, args[1], args[2]);
                    break;
                case "listRanges":
                    this.listRanges(service);
                    break;
                case "listWeights":
                    this.listWeights(service, args[1]);
                    break;
                default:
                    this.outputError("The first argument does not contain a valid method name.");
            }
        } catch (IndexOutOfBoundsException e) {
            this.outputError("Program arguments have not been specified correctly.");
            this.outputUsage("MyBMIClient <request> <parameters>");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Calculates the BMI and classification for a given adult weight and height.
     *
     * @param service The BMI web service for a client.
     * @param weight The weight of the person in kilograms.
     * @param height The height of the person in centimetres.
     */
    private void calcBMI(MyBMIServer service, String weight, String height) throws RemoteException {
        this.outputResponse(service.calcBMI(weight, height));
    }

    /**
     * Lists the BMI ranges known to the calculator.
     *
     * @param service The BMI web service for a client.
     */
    private void listRanges(MyBMIServer service) throws RemoteException {
        this.outputResponse(service.listRanges());
    }

    /**
     * Provides the ideal range of weights for a given height.
     *
     * @param service The BMI web service for a client.
     * @param height The height in centimetres.
     */
    private void listWeights(MyBMIServer service, String height) throws RemoteException {
        this.outputResponse(service.listWeights(height));
    }

    /**
     * Outputs a response to the console.
     *
     * @param response The response to output.
     */
    private void outputResponse(String response) {
        System.out.println(response);
    }

    /**
     * Outputs the usage for a particular method or program call.
     *
     * @param usage The message regarding the usage.
     */
    private void outputUsage(String usage) {
        System.out.printf("Usage: %s\n", usage);
    }

    /**
     * Outputs an error message to the console.
     *
     * @param message The error message to output.
     */
    private void outputError(String message) {
        System.out.printf("Error: %s\n", message);
    }

}
