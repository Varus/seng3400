/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 2
 *
 * MyBMIAdministrator is a command-line based client that enables the user to interact with the MyBMIAdmin web service.
 */

import localhost.axis.MyBMIAdmin_jws.*;

import java.rmi.RemoteException;

public class MyBMIAdministrator {

    /**
     * The main method called upon execution.
     *
     * @param args Command line arguments passed in by the user.
     */
    public static void main(String[] args) {
        // instantiate the class and run the new main method
        MyBMIAdministrator myBMIAdministrator = new MyBMIAdministrator();
        myBMIAdministrator.runMyBMIAdministrator(args);
    }

    /**
     * Runs the main program.
     *
     * @param args The command line arguments passed in from the main method.
     */
    public void runMyBMIAdministrator(String[] args) {
        try {
            // instantiate the arguments as a list
            //List<String> args = Arrays.asList(arguments);
            // instantiate the web service
            MyBMIAdminServiceLocator locator = new MyBMIAdminServiceLocator();
            MyBMIAdmin service = locator.getMyBMIAdmin();
            // get the method name from the arguments
            String methodName = args[0];
            /*String methodName = args.get(0);
	        Method method = myBMIAdmin.getClass().getMethod(methodName, args.subList(1, args.size() - 1));
	        method.invoke (objectToInvokeOn, params);*/
            // call the appropriate method
            switch (methodName) {
                case "addRange":
                    this.addRange(service, args[1], args[2], args[3], args[4], args[5], args[6]);
                    break;
                case "deleteRange":
                    this.deleteRange(service, args[1], args[2], args[3]);
                    break;
                case "setName":
                    this.setName(service, args[1], args[2], args[3], args[4]);
                    break;
                case "callCount":
                    this.callCount(service, args[1], args[2]);
                    break;
                default:
                    this.outputError("The first argument does not contain a valid method name.");
            }
        } catch (IndexOutOfBoundsException e) {
            this.outputError("Program arguments have not been specified correctly.");
            this.outputUsage("MyBMIAdministrator <request> <parameters>");
        } catch (RemoteException e) {
            this.outputError("Remote exception occurred.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new BMI range to the system.
     *
     * @param service The BMI web service for an administrator.
     * @param username The username of the administrator to authenticate.
     * @param password The password for the administrator account.
     * @param lower The lower bound of the BMI range being added.
     * @param upper The upper bound of the BMI range being added.
     * @param name The name of the BMI range being add.
     * @param normal Whether the BMI range being added is desirable.
     * @throws RemoteException
     */
    private void addRange(MyBMIAdmin service, String username, String password, String lower, String upper, String name, String normal) throws RemoteException {
        // check if the normal argument passed in is a boolean
        if (normal.equalsIgnoreCase("true") || normal.equalsIgnoreCase("false")) {
            // convert the normal argument to a boolean and add the new range
            boolean desirable = Boolean.parseBoolean(normal);
            boolean added = service.addRange(username, password, lower, upper, name, desirable);
            this.outputResponse(String.format("The range was %s.\n", added ? "added successfully" : "not able to be added"));
        } else {
            this.outputError("The normal argument could not be converted to a boolean.");
        }
    }

    /**
     * Removes a range of a specified name.
     *
     * @param service The BMI web service for an administrator.
     * @param username The username of the administrator to authenticate.
     * @param password The password for the administrator account.
     * @param name The name of the BMI range being removed.
     * @throws RemoteException
     */
    private void deleteRange(MyBMIAdmin service, String username, String password, String name) throws RemoteException {
        boolean deleted = service.deleteRange(username, password, name);
        this.outputResponse(String.format("The range was %s.\n", deleted ? "deleted successfully" : "not able to be deleted"));
    }

    /**
     * Updates the name of a specified range.
     *
     * @param service The BMI web service for an administrator.
     * @param username The username of the administrator to authenticate.
     * @param password The password for the administrator account.
     * @param oldName The current name of the BMI range.
     * @param newName The new name for the BMI range.
     * @throws RemoteException
     */
    private void setName(MyBMIAdmin service, String username, String password, String oldName, String newName) throws RemoteException {
        boolean altered = service.setName(username, password, oldName, newName);
        this.outputResponse(String.format("The name was %s.\n", altered ? "successfully altered" : "not able to be altered"));
    }

    /**
     * Returns the total number of calls on either of the web services.
     *
     * @param service The BMI web service for an administrator.
     * @param username The username of the administrator to authenticate.
     * @param password The password for the administrator account.
     * @throws RemoteException
     */
    private void callCount(MyBMIAdmin service, String username, String password) throws RemoteException {
        this.outputResponse(Integer.toString(service.callCount(username, password)));
    }

    /**
     * Outputs a response to the console.
     *
     * @param response The response to output.
     */
    private void outputResponse(String response) {
        System.out.println(response);
    }

    /**
     * Outputs the usage for a particular method or program call.
     *
     * @param usage The message regarding the usage.
     */
    private void outputUsage(String usage) {
        System.out.printf("Usage: %s\n", usage);
    }

    /**
     * Outputs an error message to the console.
     *
     * @param message The error message to output.
     */
    private void outputError(String message) {
        System.out.printf("Error: %s\n", message);
    }

}
