/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 3
 *
 * This Thread contains a reference to the client and demo server object to enable the use of a callback within the
 * client.
 */

package client;

import DemoApp.Demo;

public class AsynchronousThread extends Thread {

    /**
     * Member variables.
     */
    private DemoClient demoClient;
    private Demo demo;
    private volatile boolean hasResponse;

    /**
     * The constructor that sets the reference to the client and the server object.
     *
     * @param demoClient The reference to the client.
     * @param demo The interface used to call the object method within the server.
     */
    public AsynchronousThread(DemoClient demoClient, Demo demo) {
        this.demoClient = demoClient;
        this.demo = demo;
        hasResponse = false;
    }

    /**
     * Runs the deferred thread.
     */
    public void run() {
        demoClient.sync(demo.getCharacter());
        hasResponse = true;
    }

    public boolean hasResponse() {
        return hasResponse;
    }

}
