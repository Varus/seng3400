/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 3
 *
 * This is a client that demonstrates synchronous, deffered synchronous and asynchronous interactions with a server.
 */

package client;

import DemoApp.*;
import org.omg.CORBA.ORB;
import org.omg.CORBA.Request;
import org.omg.CORBA.WrongTransaction;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

public class DemoClient {

	/**
	 * Member variables.
	 */
    private ORB orb;
	private volatile Character character;

    private final Character DEFAULT_CHARACTER;
    private final int REQUEST_SERVER;
    private final int SYNC_SERVER;
    private final int CONTINUE_ITERATIONS;
    private final int DELAY;

    private final int MAX_SYNCHRONOUS_ITERATIONS;

    private final int MAX_DEFERRED_SYNCHRONOUS_ITERATIONS;

	/**
	 * Default constructor.
	 */
	public DemoClient() {
        DEFAULT_CHARACTER = '*';
        character = DEFAULT_CHARACTER;
        CONTINUE_ITERATIONS = 5;
        SYNC_SERVER = 10;
        MAX_SYNCHRONOUS_ITERATIONS = 10;
        MAX_DEFERRED_SYNCHRONOUS_ITERATIONS = 15;
        REQUEST_SERVER = 5;
        DELAY = 1000;
    }

	/**
	 * Runs the program.
	 *
	 * @param args The command-line arguments passed to the program.
	 */
	public static void main(String[] args) {
	    DemoClient demoClient = new DemoClient();
        demoClient.runDemoClient(args);
	}

    /**
     * Begins the demo client program.
     *
     * @param args The command-line arguments passed into the program.
     */
    public void runDemoClient(String[] args) {
        try {
            // create and initialise the orb
            orb = ORB.init(args, null);
            // get the root naming context
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            // get the naming context
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            // resolve the object reference
            String name = "Demo";
            Demo demo = DemoHelper.narrow(ncRef.resolve_str(name));
            System.out.println("Obtained a handle on server object.");
            // run the synchronous interaction demonstration
            this.runSynchronousInteraction(demo, MAX_SYNCHRONOUS_ITERATIONS);
            // run the deferred synchronous interaction demonstration
            this.runDeferredSynchronousInteraction(demo, MAX_DEFERRED_SYNCHRONOUS_ITERATIONS);
            // run the asynchronous interaction demonstration
            this.runAsynchronousInteraction(demo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Demonstrates the synchronous interaction between the client and server.
     *
     * @param demo The interface used to call the object method within the server.
     * @param maxIterations The maximum number of iterations to execute the loop for.
     */
    private void runSynchronousInteraction(Demo demo, int maxIterations) {
        // resets the character to its default state
        this.resetCharacter();
        // outputs the heading
        System.out.println("Demonstrating synchronous interaction.");
        // loops from the first iteration to the maximum amount
        for (int i = 1; i <= maxIterations; i++) {
            this.outputValue(i);                                    // outputs the current value of the character
            if (i == REQUEST_SERVER) {                              // checks if the server needs to be called
                this.outputCallToServer();                          // output the call to the server text
                this.setCharacter(demo.getCharacter());             // set the new character
            }
            try {
                Thread.sleep(DELAY);                                // add a delay between iterations
            } catch (InterruptedException ignored) {}
        }
    }

    /**
     * Demonstrates the deferred synchronous interaction between the client and server.
     *
     * @param demo The interface used to call the object method within the server.
     * @param maxIterations The maximum number of iterations to execute the loop for.
     */
    private void runDeferredSynchronousInteraction(Demo demo, int maxIterations) {
        // resets the character to its default state
        this.resetCharacter();
        // outputs the heading
        System.out.println("Demonstrating deferred synchronous interaction.");
        // initialise the request
        DeferredSynchronousRequest deferredSynchronousRequest = new DeferredSynchronousRequest();
        // loops from the first iteration to the maximum amount
        for (int i = 1; i <= maxIterations; i++) {
            this.outputValue(i);                                    // outputs the current value of the character
            if (i == REQUEST_SERVER) {                              // checks if the server needs to be called
                this.outputCallToServer();                          // output the heading
                deferredSynchronousRequest.makeRequest(orb, demo);  // instantiate the request
            }
            if (i == SYNC_SERVER) {                                 // check if the server needs to fetch the response
                this.outputResponse();                              // outputs getting the response from the server
                // gets the request from the class
                Request request = deferredSynchronousRequest.getRequest();
                try {
                    request.get_response();                         // synchronously gets the response from the request
                } catch (WrongTransaction ignored) {}
                // obtains the response from the request and sets the new character
                this.setCharacter(request.result().value().extract_char());
            }
            try {
                Thread.sleep(DELAY);                                // add a delay between iterations
            } catch (InterruptedException ignored) {}
        }
    }

    /**
     * Demonstrates the asynchronous interaction between the client and server.
     *
     * @param demo The interface used to call the object method within the server.
     */
    private void runAsynchronousInteraction(Demo demo) {
        // resets the character to its default state
        this.resetCharacter();
        // outputs the heading
        System.out.println("Demonstrating asynchronous interaction.");
        // initialise the request
        AsynchronousThread asynchronousThread = new AsynchronousThread(this, demo);
        // initialise the loop termination index
        int responseIndex = Integer.MAX_VALUE;
        for (int i = 1; i < responseIndex; i++) {                   // loops from the first iteration to the response index
            this.outputValue(i);                                    // outputs the current value of the character
            if (i == REQUEST_SERVER) {                              // check if the server needs to be called
                this.outputCallToServer();                          // displays the output call text
                asynchronousThread.start();                         // runs the asynchronous thread
            }
            // check if the request has a response and the index needs to be modified
            if (asynchronousThread.hasResponse() && responseIndex == Integer.MAX_VALUE) {
                responseIndex = i + CONTINUE_ITERATIONS;            // modify the response index
            }
            try {
                Thread.sleep(DELAY);                                // add a delay between iterations
            } catch (InterruptedException ignored) {}
        }
    }

    /**
     * The callback once the asynchronous thread has run.
     *
     * @param character The new value of the character gained from the request.
     */
    public void sync(Character character) {
        this.setCharacter(character);                               // sets the character from the response
        this.outputResponse();                                      // output the response text
    }

    /**
     * Outputs the text for making a call to the server.
     */
    private void outputCallToServer() {
        System.out.println("Call to server");
    }

    /**
     * Outputs the text for getting the response from the server.
     */
    private void outputResponse() {
        System.out.println("Fetch response");
    }

    /**
     * Outputs the current value of the character.
     *
     * @param iteration The current iteration within the loop.
     */
    private void outputValue(int iteration) {
        System.out.printf("%-5s Value is %c\n", String.format("%d.", iteration), this.getCharacter());
    }

    /**
     * A mutator method to change the value of the character.
     *
     * @param character The new value of the character.
     */
    public void setCharacter(Character character) {
        this.character = character;
    }

    /**
     * Assigns the default character to the member variable.
     */
    private void resetCharacter() {
        this.setCharacter(DEFAULT_CHARACTER);
    }

    /**
     * An accessor method that returns the value of the current character.
     *
     * @return The value of the current character.
     */
    public Character getCharacter() {
        return character;
    }

}
