/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 3
 *
 * A deferred syncrhonous request class that holds an instance of a CORBA request object. The sole purpose of this
 * class is to make a deferred request using the inbuilt methods in CORBA. The value can later be accessed through its
 * response.
 */

package client;

import DemoApp.Demo;
import org.omg.CORBA.*;

public class DeferredSynchronousRequest {

    /**
     * Member variables.
     */
    private Request request;

    /**
     * Creates the deferred request.
     *
     * @param orb The ORB object.
     * @param demo The interface used to call the object method within the server.
     */
    public void makeRequest(ORB orb, Demo demo) {
        request = demo._request("getCharacter");
        request.set_return_type(orb.get_primitive_tc(TCKind.tk_char));
        request.send_deferred();
    }

    /**
     * An accessor method that returns the request object.
     *
     * @return The request object.
     */
    public Request getRequest() {
        return request;
    }

}
