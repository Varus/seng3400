/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 3
 *
 * This server runs the demo object.
 */

package server;

import DemoApp.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

public class DemoServer {

    /**
     * The main method that gets called upon starting the server.
     *
     * @param args The command-line arguments passed into the program.
     */
	public static void main(String args[]) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);
			// get reference to rootpoa and activate the POAManager
			POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();
			// create servant and register it with the ORB
			DemoObj demoObj = new DemoObj(orb);
			// get object reference from the servant
			org.omg.CORBA.Object ref = rootpoa.servant_to_reference(demoObj);
			Demo href = DemoHelper.narrow(ref);
			// get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			// get the naming context
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			// bind the object reference in naming
			String name = "Demo";
			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path, href);
			System.out.println("DemoServer is ready.");
			// wait for invocations from clients
            orb.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
        System.out.println("DemoServer is now exiting...");
	}
}

