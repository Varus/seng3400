/**
 * @author Monica Olejniczak
 *
 * Student number: c3163087
 * Program: Bachelor of Software Engineering
 * Course: SENG3400 - Network and Distributed Computing
 * Task: Assignment 3
 *
 * This class implements the methods specified in the demo.idl file and is used by the server.
 */

package server;

import DemoApp.DemoPOA;
import org.omg.CORBA.ORB;

import java.util.Random;

public class DemoObj extends DemoPOA {

	/**
	 * Member variables.
	 */
	private ORB orb;
    private final int MIN_DELAY = 5000;
    private final int MAX_DELAY = 10000;

    /**
     * A constructor that takes in the orb object.
     *
     * @param orb The new orb object.
     */
    public DemoObj(ORB orb) {
        this.orb = orb;
    }

	/**
	 * Randomly generates a character from the sets ['a' ... 'z'] and ['A' ... 'Z'].
	 *
	 * @return A randomly generated alphabetic character.
	 */
	public char getCharacter() {
        try {
            Thread.sleep((long) (((MAX_DELAY - MIN_DELAY) * Math.random()) + MIN_DELAY));
        } catch (InterruptedException ignored) {}
        int random = new Random().nextInt(52);
		char base = (random < 26) ? 'A' : 'a';
		return (char)(base + random % 26);
	}

	/**
	 * A method that calls the shutdown method on the orb.
	 */
	public void shutdown() {
		orb.shutdown(false);
	}
}
