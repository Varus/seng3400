/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This class is used by both the client and the server to parse the server and port number arguments into member
 * variables for easy retrieval. It also enables these arguments to be in any order.
 *
 * The arguments must be in the form of server=<host name> port=<port number>
 */

public class ArgumentParser {

	/**
	 * Member variables
	 */
	private String server;
	private int port;

	/**
	 * The default constructor.
	 */
	public ArgumentParser() {
		server = "127.0.0.1";
		port = 9090;
	}

	/**
	 * Parses the command line arguments to override the server and port if specified by the user.
	 *
	 * @param args the arguments from the main program
	 */
	public void parse(final String... args) {
		String serverStart = "server=";
		String portStart = "port=";
		// loops through all the arguments
		for (String argument : args) {
			// checks if the argument refers to the server or the port
			if (argument.startsWith(serverStart)) {
				// changes the server
				server = argument.substring(serverStart.length(), argument.length());
			} else if (argument.startsWith(portStart)) {
				port = Integer.parseInt(argument.substring(portStart.length(), argument.length()));
			}
		}
	}

	/**
	 * An accessor method that returns the server member variable.
	 *
	 * @return the server name
	 */
	public String getServer() {
		return server;
	}

	/**
	 * An accessor method that returns the port member variable.
	 *
	 * @return the port number for the server
	 */
	public int getPort() {
		return port;
	}

}
