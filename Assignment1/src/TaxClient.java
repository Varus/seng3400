/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This program communicates with the server as a tax client. It can make requests to the server to store information
 * about specific tax scales and display them. The client can shut the server down, or close their session and display
 * the tax return for a specified amount.
 */

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class TaxClient {

	/**
	 * This method enables the client to make requests to the server.
	 *
	 * @param scanner the standard input
	 * @param input the input stream that contains the server messages
	 * @param output the output stream being used to send messages to the server
	 */
	private void makeRequests(Scanner scanner, BufferedReader input, BufferedWriter output) {
		String userInput = "";
		String format = "%-10s";
		// loops through the user input until the client wants to end their session
		while (!(userInput.equals(TaxOperations.Operation.BYE.name()) || userInput.equals(TaxOperations.Operation.END.name()))) {
			System.out.printf(format, "Client: ");
			userInput = scanner.nextLine();
			// check if inside a store command
			if (userInput.equals(TaxOperations.Operation.STORE.name())) {
				userInput = String.format("%s\n%s", userInput, this.store(scanner, format));
			}
			this.send(output, userInput);           // sends the client request to the server
			this.outputServerInformation(input);    // outputs the response from the server
		}
	}

	/**
	 * This is the method called when the client requests to store tax information.
	 *
	 * @param scanner the standard input
	 * @param format the formatted string to space out the console output
	 * @return the store string for the server
	 */
	private String store(Scanner scanner, String format) {
		// gets the store inputs from the user
		String lowerBound = getInput(scanner, format);
		String upperBound = getInput(scanner, format);
		int base = Integer.parseInt(getInput(scanner, format));
		int amountOver = Integer.parseInt(getInput(scanner, format));
		// returns the formatted store string for the server
		return String.format("%s\n%s\n%s\n%s", lowerBound, upperBound, base, amountOver);
	}

	/**
	 * This method returns the input that is requested by the user with a formatted area to push the content
	 *
	 * @param scanner the standard input
	 * @param format the formatted string to space out the console output
	 * @return the string for the specified input
	 */
	private String getInput(Scanner scanner, String format) {
		System.out.printf(format, "");
		return scanner.nextLine();
	}

	/**
	 * Sends the server the specified message.
	 *
	 * @param output the output stream being used to send messages to the server
	 * @param message the message to send to the sever
	 */
	private void send(BufferedWriter output, String message) {
		try {
			output.write(String.format("%s\n", message));   // writes to the server
			output.flush();                                 // flushes the output stream
		} catch (IOException e) {
			this.output("ERROR: Could not write to output stream.");
		}
	}

	/**
	 * Outputs the specified message
	 *
	 * @param string the string to output
	 */
	private void output(String string) {
		System.out.println(string);
	}

	/**
	 * Displays the information from the server to the client.
	 *
	 * @param input the input stream that contains the server messages
	 */
	private void outputServerInformation(BufferedReader input) {
		try {
			// read the first line and output it and then checks if there are more lines to output
			System.out.printf("%-10s%s\n", "Server: ", input.readLine());
			while (input.ready()) {
				this.output(input.readLine());
			}
		} catch (IOException e) {
			this.output("ERROR: Could not read input stream.");
		}
	}

	/**
	 * Shuts down the client and its input and output streams.
	 *
	 * @param client the client that is connected to a server
	 * @param input the input stream that contains the server messages
	 * @param output the output stream being used to send messages to the server
	 */
	private void shutdown(Socket client, BufferedReader input, BufferedWriter output) {
		try {
			input.close();          // close the input stream
			output.close();         // close the output stream
			client.close();         // close the client socket
		} catch (IOException e) {
			this.output("ERROR: Could not shutdown the client.");
		}
		System.exit(0);             // ensures the program terminates
	}

	/**
	 * This method connects the client to the specified server and begins the protocol.
	 *
	 * @param args the command line arguments for the client
	 */
	private void start(String[] args) {
		// create the argument parser instance and parse the command-line arguments
		ArgumentParser parser = new ArgumentParser();
		parser.parse(args);
		// declare the server name and port number
		String server = parser.getServer();
		int port = parser.getPort();
		try {
			// outputs to the console that the client is attempting to connect to the server
			System.out.printf("Connecting to %s on port %s\n", server, port);
			Socket client = new Socket(server, port);
			// outputs to the console that the client has connected to the server
			System.out.printf("Just connected to %s\n", client.getRemoteSocketAddress());
			// create the necessary inputs and outputs
			Scanner scanner = new Scanner(System.in);
			BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
			BufferedWriter output = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			this.makeRequests(scanner, input, output);  // enables the client to make requests
			this.shutdown(client, input, output);       // shuts down the client when they are finished making requests
		} catch(IOException e) {
			this.output("ERROR: Could not connect to the server on the specified port.");
		}
	}

	/**
	 * This is the main entry point of the program that instantiates the tax client and starts the program
	 *
	 * @param args the command line arguments for the client
	 */
	public static void main(String[] args) {
		TaxClient taxClient = new TaxClient();
		taxClient.start(args);
	}
}
