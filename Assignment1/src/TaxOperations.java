/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This class store the available tax operations the client and server can refer to in an enumeration.
 */
public class TaxOperations {

	/**
	 * Operation enumeration.
	 */
	public static enum Operation {
		TAX,
		STORE,
		QUERY,
		BYE,
		END
	}

}
