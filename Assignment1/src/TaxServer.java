/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This program communicates with the client as a tax server. It can store and display tax scales, as well as display a
 * specified tax return as requested by the client.
 */

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class TaxServer extends Thread {

	/**
	 * Member variables.
	 */
	private ServerSocket serverSocket;
	private Socket server;
	private Integer clientNumber;
	private ArrayList<TaxScale> taxScales;

	/**
	 * Constructor that connects to the server of a specific port number.
	 *
	 * @param port the port that the server is connecting to
	 * @throws IOException
	 */
	public TaxServer(int port) throws IOException {
		serverSocket = new ServerSocket(port);      // creates the server socket at the specified port
		serverSocket.setSoTimeout(100000);          // sets the server socket timeout
		clientNumber = port;                        // sets the client number to the port
		taxScales = new ArrayList<>(10);            // initialises the tax scales
	}

	/**
	 * Runs the server thread
	 */
	public void run() {
		while(!Thread.interrupted()) {
			BufferedReader input = null;
			BufferedWriter output = null;
			try {
				// outputs that the server is waiting for a client to connect and blocks processing until a client
				// has connected
				System.out.printf("MESSAGE: Waiting for client on port %s...\n", serverSocket.getLocalPort());
				server = serverSocket.accept();
				// outputs that a client has connected to the server
				System.out.printf("MESSAGE: Just connected to %s\n", server.getRemoteSocketAddress());
				// initialises the input and output streams
				input = new BufferedReader(new InputStreamReader(server.getInputStream()));
				output = new BufferedWriter(new OutputStreamWriter(server.getOutputStream()));
			} catch (IOException e) {
				// exits the system due to an error in reading the input and output streams
				this.output("ERROR: Could not connect to the client.");
				System.exit(-1);
			}
			// calls the method to initialise the client session
			this.initiateClientSession(input, output, TaxOperations.Operation.TAX);

			this.doClientRequests(input, output);   // performs the client requests
			this.closeStreams(input, output);       // closes the input and output streams
		}
		this.shutdown();                            // calls the method to shutdown the server
	}

	/**
	 * This method attempts to initialise the client's session when they input the associated command.
	 *
	 * @param input the input stream being used to receive messages from the client
	 * @param output the output stream being used to send messages to the client
	 * @param operation the command that is used by the client to create a session
	 */
	private void initiateClientSession(BufferedReader input, BufferedWriter output, TaxOperations.Operation operation) {
		// attempts to read the input line
		try {
			// checks if the input line does not equal the command
			while (!input.readLine().equals(operation.name())) {
				// outputs the invalid command and sends the information back to the client
				String error = "ERROR: Invalid command.";
				this.output(error);
				this.send(output, error);
			}
		} catch (IOException e) {
			// an error was encountered reading the input stream
			this.output("ERROR: The input could not be read.");
			System.exit(-1);
		}
		// outputs a message to the client and server indicating the session has been initialised
		this.output("MESSAGE: The session has been initialised with the client.");
		this.send(output, String.format("%s: OK", operation.name()));
	}

	/**
	 * This method reads all inputs the client submits to the server and performs each request.
	 *
	 * @param input the input stream being used to receive messages from the client
	 * @param output the output stream being used to send messages to the client
	 */
	private void doClientRequests(BufferedReader input, BufferedWriter output) {
		try {
			String request;
			// loops until the socket has been closed
			while ((request = input.readLine()) != null) {
				// performs the client request
				this.doClientRequest(input, output, request);
				// checks if the server has been closed after the request
				if (server.isClosed()) {
					break;
				}
			}
		} catch (IOException e) {
			// an error was encountered reading the input stream
			this.output("ERROR: The input could not be read.");
			System.exit(-1);
		}
	}

	/**
	 * This method reads the input the client has submitted to the server and performs the request.
	 *
	 * @param input the input stream being used to receive messages from the client
	 * @param output the output stream being used to send messages to the client
	 * @param request the command sent by the client
	 */
	private void doClientRequest(BufferedReader input, BufferedWriter output, String request) {
		// checks if the client requested for the server to perform their tax
		if (this.isInteger(request)) {
			this.request(output, Integer.parseInt(request));
		} else {
			// attempts to find which operation the client selected if they did not request their tax
			try {
				// finds the value of the request from the client
				switch (TaxOperations.Operation.valueOf(request)) {
					case STORE:
						this.store(input, output, TaxOperations.Operation.STORE);
						break;
					case QUERY:
						this.query(output, TaxOperations.Operation.QUERY);
						break;
					case BYE:
						this.bye(output, TaxOperations.Operation.BYE);
						break;
					case END:
						this.end(output, TaxOperations.Operation.END);
						break;
				}
			} catch (IllegalArgumentException e) {
				// the value does not exist and results in client error
				String error = "ERROR: Invalid command.";
				this.output(error);
				this.send(output, error);
			}
		}
	}

	/**
	 * Checks if the specified string is an integer by attempting to parse it. If an exception occurs, the string is
	 * not an integer and it returns false. Otherwise, it is an integer and returns true.
	 *
	 * @param value the value to parse as an integer
	 * @return whether the string is an integer or not
	 */
	private boolean isInteger(String value) {
		try {
			int i = Integer.parseInt(value);
		} catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	/**
	 * This is the method called when the client requests to store new information about a tax scale.
	 *
	 * @param input the input stream being used to receive messages from the client
	 * @param output the output stream being used to send messages to the client
	 * @param operation the operation used by the client
	 */
	private void store(BufferedReader input, BufferedWriter output, TaxOperations.Operation operation) {
		// begin the store exchange
		try {
			// get the values from the client
			String lowerBound = input.readLine();
			String upperBound = input.readLine();
			int base = Integer.parseInt(input.readLine());
			int cents = Integer.parseInt(input.readLine());
			// add the values to the tax scale and output its success
		    this.addTaxScale(lowerBound, upperBound, base, cents);
			this.output("MESSAGE: Store operation was successful.");
			this.send(output, String.format("%s: OK", operation.name()));
		} catch (IOException e) {
			this.output("ERROR: The store input could not be read.");
		}
	}

	/**
	 * Adds a tax scale to the array list after the client requests to store new values.
	 *
	 * @param lowerBound the lower bound of the income range
	 * @param upperBound the upper bound of the income range
	 * @param base the base amount for tax return
	 * @param cents the amount of cents to return for the amount over per dollar
	 */
	private void addTaxScale(String lowerBound, String upperBound, int base, int cents) {

		String unspecified = "~";                                   // instantiates the unspecified string
		Integer lower = null;                                       // instantiates the lower bound to null
		Integer upper = null;                                       // instantiates an upper bound to null
		if (!lowerBound.equals(unspecified)) {                      // checks if the lower bound is not specified
			lower = Integer.parseInt(lowerBound);                   // parses the lower bound if it is specified
		}
		if (!upperBound.equals(unspecified)) {                      // checks if the upper bound is not specified
			upper = Integer.parseInt(upperBound);                   // parses the upper bound if it is specified
		}
		IncomeRange incomeRange = new IncomeRange(lower, upper);    // makes a new income range for the bounds
		TaxPayable taxPayable = new TaxPayable(base, cents);        // makes a new tax payable with the amounts
		int size = taxScales.size();                                // sets the size to the size of the array list
		if (size == 0) {                                            // checks if there are no tax scales
			taxScales.add(new TaxScale(incomeRange, taxPayable));   // add the first tax scale
		} else if (lower == null && upper == null) {                // checks if both ranges are unspecified
			taxScales.clear();                                      // removes all elements in the array list
			taxScales.add(new TaxScale(incomeRange, taxPayable));   // add the tax scale
		} else {
			// loops through each tax scale
			for (int i = 0; i < size; i++) {
				// gets the current tax scale and its lower and upper bound from its income range
				TaxScale taxScale = taxScales.get(i);
				Integer currentLowerBound = taxScale.getIncomeRange().getLowerBound();
				Integer currentUpperBound = taxScale.getIncomeRange().getUpperBound();
				// checks if a scale should be added
				if (lower == null || (currentLowerBound != null && lower < currentLowerBound)) {
					// checks if the current tax scale needs to be removed
					if (upper != null && currentUpperBound != null && currentUpperBound < upper) {
						taxScales.remove(i);                                    // removes the tax scale at index i
					}
					this.addTaxScale(i, new TaxScale(incomeRange, taxPayable)); // adds the tax scale to the list
					this.updateTaxScales(upper, i + 1, taxScales.size());       // updates the tax scales from i + 1
					return;                                                     // exits the method
				}
			}
			this.addTaxScale(size, new TaxScale(incomeRange, taxPayable));      // adds the tax scale at the last index
		}
	}

	/**
	 * Adds the tax scale to the specified index and updates the previous tax scale in the list.
	 *
	 * @param i the index to add the tax scale to
	 * @param taxScale the tax scale to be added
	 */
	private void addTaxScale(int i, TaxScale taxScale) {
		// adds the tax scale to the specified index
		// updates the previous tax scale in the list
		taxScales.add(i, taxScale);
		this.updatePrevious(i, i - 1);
	}

	/**
	 * Updates the previous tax scale upper bound if it is out of date.
	 *
	 * @param currentIndex the current index of the tax scale
	 * @param previousIndex the previous index of the tax scale
	 */
	private void updatePrevious(int currentIndex, int previousIndex) {
		// check if the previous index is valid
		if (previousIndex >= 0) {
			// retrieves the current and previous tax scales
			TaxScale current = taxScales.get(currentIndex);
			TaxScale previous = taxScales.get(previousIndex);
			// gets the upper bound of the previous tax scale and the lower bound of the current tax scale
			Integer currentLowerBound = current.getIncomeRange().getLowerBound();
			// updates the previous upper bound to one less than the current lower bound
			previous.getIncomeRange().setUpperBound(currentLowerBound - 1);
		}
	}

	/**
	 * Updates the tax scales from the index after a tax scale has been added, if it was added in the middle of the
	 * array list.
	 *
	 * @param upperBound the upper bound of the tax scale that was inserted into the list
	 * @param index the index to update the tax scales from
	 * @param size the size of the array list
	 */
	private void updateTaxScales(Integer upperBound, int index, int size) {
		// check if they all tax scales need to be removed from the index onward
		if (upperBound == null) {
			// remove all elements from the index onward
			taxScales.removeAll(taxScales.subList(index, size));
		} else {
			// loops through the tax scales from the specified index onwards
			for (int i = index; i < size; i++) {
				// gets the current tax scale and its lower and upper bounds
				TaxScale taxScale = taxScales.get(i);
				Integer currentLowerBound = taxScale.getIncomeRange().getLowerBound();
				Integer currentUpperBound = taxScale.getIncomeRange().getUpperBound();
				// check if the scale needs to be updated
				if (currentLowerBound == null) {
					taxScale.getIncomeRange().setLowerBound(upperBound + 1);
				}
				// checks if the lower bound of the current tax scale income range needs to be updated
				else if (currentLowerBound < upperBound) {
					// check if the tax scale needs to be removed or updated
					if (currentUpperBound < upperBound) {
						taxScales.remove(i);                // remove the current tax scale
						i--;                                // decrement the value of i
						size--;                             // decrement the size to account for the removed tax scale
					} else {
						taxScale.getIncomeRange().setLowerBound(upperBound + 1);
					}
				}
			}
		}
	}

	/**
	 * This is the method called when the client requests to query the current tax rates.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param operation the operation used by the client
	 */
	private void query(BufferedWriter output, TaxOperations.Operation operation) {
		// declares a message string builder
		StringBuilder message = new StringBuilder();
		// ensures the first tax scale is not null
		if (taxScales.size() != 0) {
			// overrides the message with the first tax scale
			message.append(String.format("%s", taxScales.get(0).toString()));
			// loops through the remaining tax scales and appends the associated tax scale
			for (int i = 1; i < taxScales.size(); i++) {
				message.append(String.format("\n%10s%s", "", taxScales.get(i).toString()));
			}
		}
		// appends the success message to the builder, outputs the success and sends the output to the client
		message.append(String.format("\n%s: OK", operation.name()));
		this.output("MESSAGE: Query operation was successful.");
		this.send(output, message.toString());
	}

	/**
	 * This is the method called when the client requests to view the tax return for the specified value.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param value the value to find the tax return for
	 */
	private void request(BufferedWriter output, int value) {
		for (TaxScale taxScale : taxScales) {
			// checks if the tax scale is not null and proceeds if it exists
			if (taxScale != null) {
				// gets the income range of the tax scale and its lower bound
				IncomeRange incomeRange = taxScale.getIncomeRange();
				Integer lowerBound = incomeRange.getLowerBound();
				// checks if the value is in the specified range of the tax scale
				if (incomeRange.isInsideBounds(value)) {
					// performs the tax return and escapes the method
					this.doTaxReturn(output, value, lowerBound == null ? 0 : lowerBound, taxScale);
					return;
				}
			} else {
				break; // exits the loop
			}
		}
		this.output("MESSAGE: Value could not be found in request operation."); // outputs that the message
		this.send(output, String.format("I don't know %s", value));             // sends the message to the client
	}

	/**
	 * This method performs the tax return for a tax scale that lies within the bounds of its specified income range.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param value the value to find the tax return for
	 * @param lowerBound the lower bound for the income range in the tax scale
	 * @param taxScale the current tax scale
	 */
	private void doTaxReturn(BufferedWriter output, int value, int lowerBound, TaxScale taxScale) {
		// calculates the tax return, outputs the successful operation and sends the data to the client
		int tax = this.calculateTaxReturn(value, lowerBound, taxScale.getTaxPayable());
		this.output("MESSAGE: Request operation successful.");
		this.send(output, String.format("Tax is %s", tax));
	}

	/**
	 * Calculates the tax return by first working out the difference between the value and the lower bound -1. This is
	 * used for working out how many dollars are over this particular tax payable tax scale. The formula then adds the
	 * base amount with the difference multiplied by the amount in cents. It converts this amount to dollars by
	 * dividing it by 100.
	 *
	 * @param value the value to find the tax return for
	 * @param lowerBound the lower bound for the income range in the tax scale
	 * @param taxPayable the tax payable object that contains the base amount and its cents
	 * @return the tax return value
	 */
	private int calculateTaxReturn(int value, int lowerBound, TaxPayable taxPayable) {
		int difference = value - (lowerBound - 1);
		return taxPayable.getBaseAmount() + (difference * taxPayable.getCents() / 100);
	}

	/**
	 * This is the method called when the client requests to end their session with the server.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param operation the operation used by the client
	 */
	private void bye(BufferedWriter output, TaxOperations.Operation operation) {
		// outputs to the client that the message has been received
		this.send(output, String.format("%s: OK", operation.name()));
		// attempts to close the connection and outputs that it has been closed if successful
		try {
			server.close();
			System.out.printf("MESSAGE: Connection with client %s is now closed.\n", clientNumber);
		} catch (IOException e) {
			this.output("ERROR: The connection with the client could not be closed.");
		}
	}

	/**
	 * This is the method called when the client requests to end their session and the server's.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param operation the operation used by the client
	 */
	private void end(BufferedWriter output, TaxOperations.Operation operation) {
		// sends the message to the client and interrupts the current thread
		this.send(output, String.format("%s: OK", operation.name()));
		this.interrupt();
	}

	/**
	 * Sends the client the specified message.
	 *
	 * @param output the output stream being used to send messages to the client
	 * @param message the message to send to the client
	 */
	private void send(BufferedWriter output, String message) {
		try {
			// writes to the client and flushes the output stream
			output.write(String.format("%s\n", message.toUpperCase()));
			output.flush();
		} catch (IOException e) {
			this.output("ERROR: Could not write to output stream."); // displays the error message
		}
	}

	/**
	 * Closes the specified input and output streams.
	 *
	 * @param input the input stream being used to receive messages from the client
	 * @param output the output stream being used to send messages to the client
	 */
	private void closeStreams(BufferedReader input, BufferedWriter output) {
		try {
			input.close();          // closes the input stream
			output.close();         // closes the output stream
		} catch (IOException e) {
			this.output("ERROR: The input and output streams could not be closed.");
		}
	}

	/**
	 * Shuts down the server and server socket
	 */
	private void shutdown() {
		try {
			server.close();                                         // closes the server
			serverSocket.close();                                   // closes the server socket
			this.output("MESSAGE: The server has been shutdown.");  // outputs the shut down message
		} catch (IOException e) {
			this.output("ERROR: The server could not shutdown.");   // outputs the error message
		}
		System.exit(0);                                             // ensures the program terminates
	}

	/**
	 * Outputs the specified message
	 *
	 * @param string the string to output
	 */
	private void output(String string) {
		System.out.println(string);
	}

	/**
	 * Begins the TaxServer server thread with the default or overridden port.
	 *
	 * @param args the arguments of the program that can override the port number
	 */
	public static void main(String[] args) {
		ArgumentParser parser = new ArgumentParser();   // creates the argument parser instance
		parser.parse(args);                             // parses the command-line arguments
		int port = parser.getPort();                    // declare the port number
		try {
			Thread thread = new TaxServer(port);        // creates the server at the specified port
			thread.start();                             // starts the server
		} catch(IOException e) {
			System.out.println("ERROR: The thread could not be started.");
		}
	}
}
