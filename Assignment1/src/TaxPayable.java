/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This class stores the base amount for a tax return and the amount in cents that is returnable per dollar over a
 * certain threshold.
 */

public class TaxPayable {

	/**
	 * Member variables.
	 */
	private int baseAmount;
	private int cents;

	/**
	 * This constructor sets both the baseAmount and amount over a certain threshold.
	 *
	 * @param base the baseAmount amount that is tax payable
	 * @param cents the amount of cents to be paid per dollar over a certain threshold
	 */
	public TaxPayable(int base, int cents) {
		this.baseAmount = base;
		this.cents = cents;
	}

	/**
	 * This accessor method returns the baseAmount amount that is tax payable.
	 *
	 * @return the baseAmount amount that is tax payable
	 */
	public int getBaseAmount() {
		return baseAmount;
	}

	/**
	 * This accessor method returns the amount of cents to be paid per dollar over a certain threshold.
	 *
	 * @return the amount of cents to be paid per dollar over a certain threshold
	 */
	public int getCents() {
		return cents;
	}

	/**
	 * @return the tax payable string that comprises of the base amount and amount over
	 */
	@Override
	public String toString() {
		return String.format("%s %s", baseAmount, cents);
	}

}
