/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This class stores the information appropriate information for a tax scale, which includes the data for an income
 * range and the data for the tax payable.
 */

public class TaxScale {

	/**
	 * Member variables.
	 */
	private IncomeRange incomeRange;
	private TaxPayable taxPayable;

	/**
	 * This constructor sets the income range and the amount payable for the tax scale.
	 *
	 * @param incomeRange the income range for the tax scale
	 * @param taxPayable the amount payable for the tax scale
	 */
	public TaxScale(IncomeRange incomeRange, TaxPayable taxPayable) {
		this.incomeRange = incomeRange;
		this.taxPayable = taxPayable;
	}

	/**
	 * This mutator method sets the income range for the tax scale.
	 *
	 * @param incomeRange the income range for the tax scale
	 */
	public void setIncomeRange(IncomeRange incomeRange) {
		this.incomeRange = incomeRange;
	}

	/**
	 * This mutator method sets the amount payable for the tax scale.
	 *
	 * @param taxPayable the amount payable for the tax scale
	 */
	public void setTaxPayable(TaxPayable taxPayable) {
		this.taxPayable = taxPayable;
	}

	/**
	 * This accessor method returns the the income range for the tax scale.
	 *
	 * @return the income range for the tax scale
	 */
	public IncomeRange getIncomeRange() {
		return incomeRange;
	}

	/**
	 * This accessor method returns the amount payable for the tax scale.
	 *
	 * @return the amount payable for the tax scale
	 */
	public TaxPayable getTaxPayable() {
		return taxPayable;
	}

	/**
	 * @return the tax scale string comprising of the income range and amount that is tax payable
	 */
	@Override
	public String toString() {
		return String.format("%s %s", incomeRange.toString(), taxPayable.toString());
	}

}
