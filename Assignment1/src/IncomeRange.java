/**
 * @author Monica Olejniczak
 * Student number: c3163087
 * Course: SENG3400 - Network and Distributed Computing
 *         Software Engineering
 *
 * This class stores the upper and lower bound for a income range within a tax scale.
 */

public class IncomeRange {

	/**
	 * Member variables.
	 */
	private Integer lowerBound;
	private Integer upperBound;

	/**
	 * This constructor sets both the upper and lower bound of the income range.
	 *
	 * @param lowerBound the lower bound for the income range
	 * @param upperBound the upper bound for the income range
	 */
	public IncomeRange(Integer lowerBound, Integer upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public void setLowerBound(Integer lowerBound) {
		this.lowerBound = lowerBound;
	}

	public void setUpperBound(Integer upperBound) {
		this.upperBound = upperBound;
	}

	/**
	 * This accessor method returns the lower bound for the income range.
	 *
	 * @return the lower bound for the income range
	 */
	public Integer getLowerBound() {
		return lowerBound;
	}

	/**
	 * This accessor method returns the upper bound for the income range.
	 *
	 * @return the upper bound for the income range
	 */
	public Integer getUpperBound() {
		return upperBound;
	}

	/**
	 * This method returns whether the requested value lies within the income range bounds.
	 *
	 * @param value the value to check if it lies within the lower and upper bounds of the income range
	 * @return whether the value is within the income range bounds
	 */
	public boolean isInsideBounds(int value) {
		// checks which bounds are null
		if (lowerBound == null && upperBound == null) {
			return true; // always true	since there are no bounds
		}
		else if (lowerBound == null) {
			return value <= upperBound;     // returns if the values is less than or equal to the upper bound
		} else if (upperBound == null) {
			return value >= lowerBound;     // returns if the value is more than or equal to the upper bound
		} else {
			// returns if the value is more than or equal to the lower bound and less than or equal to the upper bound
			return value >= lowerBound && value <= upperBound;
		}
	}

	/**
	 * @return the lower bound and upper bound income range string
	 */
	@Override
	public String toString() {
		String undefined = "~";
		return String.format("%s %s", lowerBound == null ? undefined : lowerBound,
				upperBound == null ? undefined : upperBound);
	}

}
